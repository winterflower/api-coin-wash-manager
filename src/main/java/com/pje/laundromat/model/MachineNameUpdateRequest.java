package com.pje.laundromat.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MachineNameUpdateRequest {
    private String machineName;
}

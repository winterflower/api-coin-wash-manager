package com.pje.laundromat.model;

import com.pje.laundromat.entity.UsageDetails;
import com.pje.laundromat.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetailsItem {
    private Long usageDetailsId;

    private LocalDateTime dateUsage;

    private Long machineId;

    private String machineFullName;

    private Long memberId;

    private String memberName;

    private String memberPhone;

    private LocalDate memberBirthday;

    private Boolean memberIsEnable;

    private LocalDateTime memberDateJoin;

    private LocalDateTime memberDateWithdrawal;


    private UsageDetailsItem(UsageDetailsItemBuilder builder) {
        this.usageDetailsId = builder.usageDetailsId;
        this.dateUsage = builder.dateUsage;
        this.machineId = builder.machineId;
        this.machineFullName = builder.machineFullName;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.memberBirthday = builder.memberBirthday;
        this.memberIsEnable = builder.memberIsEnable;
        this.memberDateJoin = builder.memberDateJoin;
        this.memberDateWithdrawal = builder.memberDateWithdrawal;
    }

    public static class UsageDetailsItemBuilder implements CommonModelBuilder<UsageDetailsItem> {
        private final Long usageDetailsId;
        private final LocalDateTime dateUsage;
        private final Long machineId;
        private final String machineFullName;
        private final Long memberId;
        private final String memberName;
        private final String memberPhone;
        private final LocalDate memberBirthday;
        private final Boolean memberIsEnable;
        private final LocalDateTime memberDateJoin;
        private final LocalDateTime memberDateWithdrawal;

        public UsageDetailsItemBuilder(UsageDetails usageDetails) {
            this.usageDetailsId = usageDetails.getId();
            this.dateUsage = usageDetails.getDateUsage();
            this.machineId = usageDetails.getMachine().getMachineId();
            this.machineFullName = usageDetails.getMachine().getMachineType().getName() + " " +usageDetails.getMachine().getMachineName();
            this.memberId = usageDetails.getMember().getMemberId();
            this.memberName = usageDetails.getMember().getMemberName();
            this.memberPhone = usageDetails.getMember().getMemberPhone();
            this.memberBirthday = usageDetails.getMember().getBirthday();
            this.memberIsEnable = usageDetails.getMember().getIsEnable();
            this.memberDateJoin = usageDetails.getMember().getDateJoin();
            this.memberDateWithdrawal = usageDetails.getMember().getDateWithdrawal();
        }

        @Override
        public UsageDetailsItem build() {
            return new UsageDetailsItem(this);
        }
    }
}

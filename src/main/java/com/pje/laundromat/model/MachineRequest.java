package com.pje.laundromat.model;

import com.pje.laundromat.enums.MachineType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MachineRequest {
    @ApiModelProperty(notes = "기계 타입", required = true)
    @Enumerated(value = EnumType.STRING)
    private MachineType machineType;

    @ApiModelProperty(notes = "기계 이름(~15글자)", required = true)
    @Length(max = 15)
    @NotNull
    private String machineName;

    @ApiModelProperty(notes = "기계 브랜드(~20글자)", required = true)
    @Length(max = 20)
    @NotNull
    private String machineBrand;

    @ApiModelProperty(notes = "기계 용량", required = true)
    @NotNull
    private Long volume;

    @ApiModelProperty(notes = "기계 구입일", required = true)
    @NotNull
    private LocalDate buyDate;

    @ApiModelProperty(notes = "기계 가격", required = true)
    @NotNull
    private Double machinePrice;
}

package com.pje.laundromat.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LoginUserRequest {
    @ApiModelProperty(notes = "회원 아이디(8~20글자)", required = true)
    @Length(min = 5, max = 20)
    @NotNull
    private String username;

    @ApiModelProperty(notes = "회원 비밀번호(8~20글자)", required = true)
    @Length(min = 5, max = 20)
    @NotNull
    private String password;
}

package com.pje.laundromat.model;

import com.pje.laundromat.entity.Member;
import com.pje.laundromat.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    private Long memberId;

    private String memberName;

    private LocalDate birthday;

    private String memberPhone;

    private String isEnable;

    private LocalDateTime dateJoin;

    private LocalDateTime dateWithdrawal;


    private MemberItem(MemberItemBuilder builder) {
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.birthday = builder.birthday;
        this.memberPhone = builder.memberPhone;
        this.isEnable = builder.isEnable;
        this.dateJoin = builder.dateJoin;
        this.dateWithdrawal = builder.dateWithdrawal;
    }

    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final Long memberId;
        private final String memberName;
        private final LocalDate birthday;
        private final String memberPhone;
        private final String isEnable;
        private final LocalDateTime dateJoin;
        private final LocalDateTime dateWithdrawal;

        public MemberItemBuilder(Member member) {
            this.memberId = member.getMemberId();
            this.memberName = member.getMemberName();
            this.birthday = member.getBirthday();
            this.memberPhone = member.getMemberPhone();
            this.isEnable = member.getIsEnable() ? "예" : "아니오" ;
            this.dateJoin = member.getDateJoin();
            this.dateWithdrawal = member.getDateWithdrawal();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}

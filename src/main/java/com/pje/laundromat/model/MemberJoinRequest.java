package com.pje.laundromat.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {
    @ApiModelProperty(notes = "회원 이름(~20글자)", required = true)
    @Length(max = 20)
    @NotNull
    private String memberName;

    @ApiModelProperty(notes = "회원 생년월일", required = true)
    @NotNull
    private LocalDate birthday;

    @ApiModelProperty(notes = "회원 연락처(~15글자)", required = true)
    @Length(max = 15)
    @NotNull
    private String memberPhone;

}

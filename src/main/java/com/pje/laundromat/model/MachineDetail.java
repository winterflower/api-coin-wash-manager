package com.pje.laundromat.model;

import com.pje.laundromat.entity.Machine;
import com.pje.laundromat.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineDetail {
    private Long id;

    private String machineTypeName;

    private String machineName;

    private LocalDate buyDate;

    private Double machinePrice;

    private MachineDetail(MachineDetailBuilder builder) {
        this.id = builder.id;
        this.machineTypeName = builder.machineTypeName;
        this.machineName = builder.machineName;
        this.buyDate = builder.buyDate;
        this.machinePrice = builder.machinePrice;

    }

    public static class MachineDetailBuilder implements CommonModelBuilder<MachineDetail> {
        private final Long id;
        private final String machineTypeName;
        private final String machineName;
        private final LocalDate buyDate;
        private final Double machinePrice;

        public MachineDetailBuilder(Machine machine) {
            this.id = machine.getMachineId();
            this.machineTypeName = machine.getMachineType().getName();
            this.machineName = machine.getMachineName();
            this.buyDate = machine.getBuyDate();
            this.machinePrice = machine.getMachinePrice();
        }

        @Override
        public MachineDetail build() {
            return new MachineDetail(this);
        }
    }
}
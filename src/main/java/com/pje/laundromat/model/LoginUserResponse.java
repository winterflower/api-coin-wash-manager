package com.pje.laundromat.model;

import com.pje.laundromat.entity.LoginUser;
import com.pje.laundromat.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginUserResponse {
    @ApiModelProperty(notes = "회원시퀀스")
    private Long memberId;

    private LoginUserResponse(LoginUserItemBuilder builder) {
        this.memberId = builder.memberId;
    }

    public static class LoginUserItemBuilder implements CommonModelBuilder<LoginUserResponse> {
        private final Long memberId;

        public LoginUserItemBuilder(LoginUser loginUser) {
            this.memberId = loginUser.getId();
        }

        @Override
        public LoginUserResponse build() {
            return new LoginUserResponse(this);
        }
    }
}

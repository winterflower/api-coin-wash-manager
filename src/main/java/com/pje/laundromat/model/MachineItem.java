package com.pje.laundromat.model;

import com.pje.laundromat.entity.Machine;
import com.pje.laundromat.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineItem {
    private Long id;

    private String machineFullName;

    private LocalDate buyDate;

    private MachineItem(MachineItemBuilder builder) {
        this.id = builder.id;
        this.machineFullName = builder.machineFullName;
        this.buyDate = builder.buyDate;

    }

    public static class MachineItemBuilder implements CommonModelBuilder<MachineItem> {
        private final Long id;
        private final String machineFullName;
        private final LocalDate buyDate;

        public MachineItemBuilder(Machine machine) {
            this.id = machine.getMachineId();
            this.machineFullName = "[" + machine.getMachineType().getName() + "] " + machine.getMachineName() + " 번";
            this.buyDate = machine.getBuyDate();
        }

        @Override
        public MachineItem build() {
            return new MachineItem(this);
        }
    }
}

package com.pje.laundromat.entity;

import com.pje.laundromat.interfaces.CommonModelBuilder;
import com.pje.laundromat.model.LoginUserRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20, unique = true)
    private String username;

    @Column(nullable = false, length = 20)
    private String password;

    private LoginUser(LoginUserBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
    }

    public static class LoginUserBuilder implements CommonModelBuilder<LoginUser> {

        private final String username;
        private final String password;

        public LoginUserBuilder(LoginUserRequest request) {
            this.username = request.getUsername();
            this.password = request.getPassword();
        }

        @Override
        public LoginUser build() {
            return new LoginUser(this);
        }
    }
}

package com.pje.laundromat.entity;

import com.pje.laundromat.interfaces.CommonModelBuilder;
import com.pje.laundromat.model.MemberJoinRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long memberId;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false)
    private LocalDate birthday;

    @Column(nullable = false, length = 15)
    private String memberPhone;

    @Column(nullable = false)
    private Boolean isEnable;
    //회원한테 받을 필요 없음 -> 시스템에서 관리

    @Column(nullable = false)
    private LocalDateTime dateJoin;
    //회원한테 받을 필요 없음 -> 시스템에서 관리

    private LocalDateTime dateWithdrawal;
    //필수가 아님. 빌더에서 세터 역할을 해주는건데 필수가 아닌 데이터는?
    //데이터를 분석해보면 가입과 동시에 탈퇴는 일반적이지 않으므로 빌더에 넣지 않는다.

    public void putWithdrawal() {
        this.isEnable = false;
        this.dateWithdrawal = LocalDateTime.now();
    }

    private Member(MemberBuilder builder) {
        this.memberName = builder.memberName;
        this.birthday = builder.birthday;
        this.memberPhone = builder.memberPhone;
        this.isEnable = builder.isEnable;
        this.dateJoin = builder.dateJoin;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String memberName;
        private final LocalDate birthday;
        private final String memberPhone;
        private final Boolean isEnable;
        private final LocalDateTime dateJoin;

        public MemberBuilder(MemberJoinRequest joinRequest) {
            this.memberName = joinRequest.getMemberName();
            this.birthday = joinRequest.getBirthday();
            this.memberPhone = joinRequest.getMemberPhone();
            this.isEnable = true;
            this.dateJoin = LocalDateTime.now();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}

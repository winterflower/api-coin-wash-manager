package com.pje.laundromat.entity;

import com.pje.laundromat.enums.MachineType;
import com.pje.laundromat.interfaces.CommonModelBuilder;
import com.pje.laundromat.model.MachineNameUpdateRequest;
import com.pje.laundromat.model.MachineRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Machine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long machineId;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private MachineType machineType;

    @Column(nullable = false, length = 15)
    private String machineName;

    @Column(nullable = false, length = 20)
    private String machineBrand;

    @Column(nullable = false)
    private Long volume;

    @Column(nullable = false)
    private LocalDate buyDate;

    @Column(nullable = false)
    private Double machinePrice;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;


    public void putDataName(MachineNameUpdateRequest updateRequest) {
        this.machineName = updateRequest.getMachineName();
        this.dateUpdate = LocalDateTime.now();
    }

    private Machine(MachineBuilder builder) {
        this.machineType = builder.machineType;
        this.machineName = builder.machineName;
        this.machineBrand = builder.machineBrand;
        this.volume = builder.volume;
        this.buyDate = builder.buyDate;
        this.machinePrice = builder.machinePrice;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MachineBuilder implements CommonModelBuilder<Machine> {
        private final MachineType machineType;
        private final String machineName;
        private final String machineBrand;
        private final Long volume;
        private final LocalDate buyDate;
        private final Double machinePrice;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MachineBuilder(MachineRequest request) {
            this.machineType = request.getMachineType();
            this.machineName = request.getMachineName();
            this.machineBrand = request.getMachineBrand();
            this.volume = request.getVolume();
            this.buyDate = request.getBuyDate();
            this.machinePrice = request.getMachinePrice();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Machine build() {
            return new Machine(this);
        }
    }
}

package com.pje.laundromat.controller;

import com.pje.laundromat.entity.Machine;
import com.pje.laundromat.entity.Member;
import com.pje.laundromat.model.CommonResult;
import com.pje.laundromat.model.ListResult;
import com.pje.laundromat.model.UsageDetailsItem;
import com.pje.laundromat.model.UsageDetailsRequest;
import com.pje.laundromat.service.MachineService;
import com.pje.laundromat.service.MemberService;
import com.pje.laundromat.service.ResponseService;
import com.pje.laundromat.service.UsageDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "이용 내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/V1/usage-details")
public class UsageDetailsController {
    private final MemberService memberService;
    private final MachineService machineService;
    private final UsageDetailsService usageDetailsService;

    @ApiOperation(value = "이용내역 등록하기")
    @PostMapping("/new")
    public CommonResult setUsageDetails(@RequestBody @Valid UsageDetailsRequest usageDetailsRequest) {
        Member member = memberService.getMemberData(usageDetailsRequest.getMemberId());
        Machine machine = machineService.getMachineData(usageDetailsRequest.getMachineId());

        usageDetailsService.setUsageDetails(member, machine, usageDetailsRequest.getDateUsage());

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "이용내역 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<UsageDetailsItem> getUsageDetails(
            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "dateStart") LocalDate dateStart,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "dateEnd") LocalDate dateEnd
    ) {
        return ResponseService.getListResult(usageDetailsService.getUsageDetails(dateStart, dateEnd), true);
    }
}

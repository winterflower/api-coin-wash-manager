package com.pje.laundromat.controller;

import com.pje.laundromat.model.*;
import com.pje.laundromat.service.MemberService;
import com.pje.laundromat.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "멤버 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/V1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "회원 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest joinRequest) {
        memberService.setMember(joinRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<MemberItem> getMembers(@RequestParam(value = "isEnable", required = false) Boolean isEnable) {
        if (isEnable == null) {
            return ResponseService.getListResult(memberService.getMembers(), true);
        } else {
            return ResponseService.getListResult(memberService.getMembers(isEnable), true);
        }
    }

    @ApiOperation(value = "회원 정보 삭제하기") //삭제하는 척하기
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "회원 시퀀스", required = true)
    })
    @DeleteMapping("/{id}")
    public CommonResult delMember(@PathVariable long id) {
        memberService.putMemberWithdrawal(id);
        return ResponseService.getSuccessResult();
    }
}

package com.pje.laundromat.controller;

import com.pje.laundromat.model.*;
import com.pje.laundromat.service.LoginUserService;
import com.pje.laundromat.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "로그인유저 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/V1/member")
public class LoginUserController {
    private final LoginUserService loginUserService;

    @ApiOperation(value = "로그인유저 등록하기")
    @PostMapping("/join")
    public CommonResult setUser(@RequestBody @Valid LoginUserRequest loginUserRequest) {
        loginUserService.setUser(loginUserRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "로그인유저 리스트 가져오기")
    @PostMapping("/login")
    public SingleResult<LoginUserResponse> getUsers(@RequestBody @Valid LoginUserRequest loginUserRequest) {
        return ResponseService.getSingleResult(loginUserService.getUser(loginUserRequest));
    }
}

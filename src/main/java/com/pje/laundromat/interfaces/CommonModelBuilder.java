package com.pje.laundromat.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}

package com.pje.laundromat.service;

import com.pje.laundromat.entity.Machine;
import com.pje.laundromat.enums.MachineType;
import com.pje.laundromat.exception.CMissingDataException;
import com.pje.laundromat.model.*;
import com.pje.laundromat.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    /** 기계 시퀀스 id로 데이터 가져오기
     *
     * @param id 기계 시퀀스를 받는다
     * @return 기계 데이터 반환
     */
    public Machine getMachineData(long id) {
        return machineRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /** 기계 등록하기
     *
     * @param request MachineRequest 항목값을 받는다
     */
    public void setMachine(MachineRequest request) {
        Machine machine = new Machine.MachineBuilder(request).build();
        machineRepository.save(machine);
    }

    /** 기계 정보 가져오기(단수)
     *
     * @param id 기계 시퀀스를 받는다
     * @return 기계 정보(단수)를 반환
     */
    public MachineDetail getMachine(long id) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MachineDetail.MachineDetailBuilder(machine).build();
    }

    /** 기계 리스트 가져오기
     *
     * @return 기계 리스트 반환
     */
    public ListResult<MachineItem> getMachines() {
        List<Machine> machines = machineRepository.findAll();
        List<MachineItem> result = new LinkedList<>();
//        for (Machine machine : machines) {
//            MachineItem machineItem = new MachineItem.MachineItemBuilder(machine).build();
//            result.add(machineItem);
//        }
        machines.forEach(machine -> {
            MachineItem machineItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(machineItem);
        });

        return ListConvertService.settingResult(result);
    }

    /** 기계 리스트 가져오기 (machineType 별로 조회)
     *
     * @param machineType 기계 타입을 받는다
     *@return 기계 리스트 반환
     */
    public ListResult<MachineItem> getMachines(MachineType machineType) {
        List<Machine> machines = machineRepository.findAllByMachineTypeOrderByMachineIdDesc(machineType);
        List<MachineItem> result = new LinkedList<>();
        machines.forEach(machine -> {
            MachineItem machineItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(machineItem);
        });
        return ListConvertService.settingResult(result);
    }

    /** 기계 이름 수정하기
     *
     * @param id 기계 시퀀스를 받는다
     * @param updateRequest updateRequest 항목값을 받는다
     */
    public void putMachineName(long id, MachineNameUpdateRequest updateRequest) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putDataName(updateRequest);
        machineRepository.save(machine);
    }

}

package com.pje.laundromat.service;

import com.pje.laundromat.entity.Machine;
import com.pje.laundromat.entity.Member;
import com.pje.laundromat.entity.UsageDetails;
import com.pje.laundromat.model.ListResult;
import com.pje.laundromat.model.UsageDetailsItem;
import com.pje.laundromat.repository.UsageDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailsService {
    private final UsageDetailsRepository usageDetailsRepository;

    /** 이용내역 등록하기
     *
     * @param member 회원을 받는다
     * @param machine 기계를 받는다
     * @param dateUsage 사용 날짜를 받는다
     */
    public void setUsageDetails(Member member, Machine machine, LocalDateTime dateUsage) {
        UsageDetails usageDetails = new UsageDetails.UsageDetailsBuilder(machine, member, dateUsage).build();
        usageDetailsRepository.save(usageDetails);
    }

    /** 이용내역 리스트 가져오기
     *
     * @param dateStart 검색 시작일을 받는다
     * @param dateEnd 검색 종료일을 받는다
     * @return 리스트 반환
     */
    public ListResult<UsageDetailsItem> getUsageDetails(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,0,0
        );
        LocalDateTime dateEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,59,59
        );

        List<UsageDetails> usageDetails = usageDetailsRepository.findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);

        List<UsageDetailsItem> result = new LinkedList<>();
        usageDetails.forEach(usageDetail -> {
            UsageDetailsItem addItem = new UsageDetailsItem.UsageDetailsItemBuilder(usageDetail).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);

    }


}

package com.pje.laundromat.service;

import com.pje.laundromat.entity.LoginUser;
import com.pje.laundromat.exception.CNoMemberDataException;
import com.pje.laundromat.model.LoginUserRequest;
import com.pje.laundromat.model.LoginUserResponse;
import com.pje.laundromat.repository.LoginUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginUserService {

    private final LoginUserRepository loginUserRepository;

    /** 유일한 회원인지 유효성 검사
     *
     * @param username 회원아이디를 받는다
     * @return 회원 수를 반환한다
     */
    private boolean isDuplicateUser(String username) {
        long dupCount = loginUserRepository.countByUsername(username);

        return dupCount <= 1;
    }

    /** 로그인 유저를 등록하기
     *
     * @param loginUserRequest loginUserRequest 항목값을 받는다
     */
    public void setUser(LoginUserRequest loginUserRequest) {
        if (!isDuplicateUser(loginUserRequest.getUsername())) throw new CNoMemberDataException();

        LoginUser loginUser = new LoginUser.LoginUserBuilder(loginUserRequest).build();
        loginUserRepository.save(loginUser);
    }

    /** 로그인 유저를 가져오기
     *
     * @param loginUserRequest loginUserRequest 항목값을 받는다
     * @return 로그인 유저값을 반환
     */
    public LoginUserResponse getUser(LoginUserRequest loginUserRequest) {
        LoginUser loginUser = loginUserRepository.findByUsername(loginUserRequest.getUsername()).orElseThrow(CNoMemberDataException::new);

        if (!loginUser.getPassword().equals(loginUserRequest.getPassword())) throw new CNoMemberDataException();

        return new LoginUserResponse.LoginUserItemBuilder(loginUser).build();
    }
}
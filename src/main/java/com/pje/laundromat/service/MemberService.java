package com.pje.laundromat.service;

import com.pje.laundromat.entity.Member;
import com.pje.laundromat.exception.CMissingDataException;
import com.pje.laundromat.exception.CNoMemberDataException;
import com.pje.laundromat.model.ListResult;
import com.pje.laundromat.model.MemberItem;
import com.pje.laundromat.model.MemberJoinRequest;
import com.pje.laundromat.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    /** 회원 시퀀스 id로 데이터 가져오기
     *
     * @param id 회원 시퀀스를 받는다
     * @return 회원 데이터 반환
     */
    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /** 회원 등록하기
     *
     * @param joinRequest joinRequest 항목값을 받는다
     */
    public void setMember(MemberJoinRequest joinRequest) {
        Member member = new Member.MemberBuilder(joinRequest).build();
        memberRepository.save(member);
    }

    /** 회원 리스트 가져오기
     *
     * @return 회원 리스트를 반환
     */
    public ListResult<MemberItem> getMembers() {
        List<Member> members = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /** 회원 리스트 가져오기 (유효 회원 여부)
     *
     * @param isEnable 유효한 회원여부 받는다
     * @return 회원 리스트 반환
     */
    public ListResult<MemberItem> getMembers(Boolean isEnable) {
        List<Member> members = memberRepository.findAllByIsEnableOrderByMemberIdDesc(isEnable);
        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /** 회원 탈퇴하기
     *
     * @param id 회원 시퀀스를 받는다
     */
    public void putMemberWithdrawal(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!member.getIsEnable()) throw new CNoMemberDataException();

        member.putWithdrawal();
        memberRepository.save(member);
    }

}

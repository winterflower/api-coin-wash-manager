package com.pje.laundromat.repository;

import com.pje.laundromat.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Long> {
    List<Member> findAllByIsEnableOrderByMemberIdDesc(Boolean isEnable);
}

package com.pje.laundromat.repository;

import com.pje.laundromat.entity.LoginUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoginUserRepository extends JpaRepository<LoginUser, Long> {

    long countByUsername(String username);

    Optional<LoginUser> findByUsername (String username);
}

package com.pje.laundromat.repository;

import com.pje.laundromat.entity.Machine;
import com.pje.laundromat.enums.MachineType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineRepository extends JpaRepository<Machine, Long> {
    List<Machine> findAllByMachineTypeOrderByMachineIdDesc(MachineType machineType);
}
